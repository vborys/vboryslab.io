---
title: Програмування
subtitle: Python, C++ та інше
bigimg: [{src: "/img/triangle_programming.jpg"}]
comments: false
---


### Python. Вчимося разом

Відео-курс з основ програмування на базі мови Python 3 розроблений для початківців та з базовим нахилом до використання середовища операційних систем сімейства GNU/Linux. 

[![alt](/img/projects/programming/pic01.png)](https://sites.google.com/view/py3course)



### Навчальний курс програмування мовою С++

Електронний ресурс (посібник) з основ програмування на базі мови С++.

[![alt](/img/projects/programming/pic02.png)](https://vborys.gitlab.io/cppcourse/)