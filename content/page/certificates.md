---
title: Самоосвіта
subtitle: Сертифікати дистанційних курсів, тренінгів та вебінарів
comments: false
---
## 2019 та раніше
{{< gallery caption-effect="fade" >}}
  {{< figure thumb="" link="/img/certificates/cert01.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert02.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert03.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert04.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert05.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert06.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert07.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert08.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert08.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert10.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert11.jpg" >}}
{{< /gallery >}}

## 2020
{{< gallery caption-effect="fade" >}}
  {{< figure thumb="" link="/img/certificates/cert12.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert13.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert14.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert15.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert16.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert17.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert18.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert19.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert20.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert21.png" >}}
{{< /gallery >}}

## 2021
{{< gallery caption-effect="fade" >}}
  {{< figure thumb="" link="/img/certificates/cert22.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert23.png" >}}  
  {{< figure thumb="" link="/img/certificates/cert24.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert25.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert26.jpg" >}}
{{< /gallery >}}

## 2022
{{< gallery caption-effect="fade" >}}
  {{< figure thumb="" link="/img/certificates/cert27.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert28.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert29.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert30.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert31.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert32.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert33.png" >}}
{{< /gallery >}}

## 2023
{{< gallery caption-effect="fade" >}}
  {{< figure thumb="" link="/img/certificates/cert34.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert35.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert36.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert37.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert38.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert39.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert40.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert41.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert42.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert43.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert44.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert45.jpg" >}}
{{< /gallery >}}

## 2024
{{< gallery caption-effect="fade" >}}
  {{< figure thumb="" link="/img/certificates/cert46.png" >}}
  {{< figure thumb="" link="/img/certificates/cert47.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert48.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert49.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert50.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert51.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert52.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert53.jpg" >}}
  {{< figure thumb="" link="/img/certificates/cert54.png" >}}
  {{< figure thumb="" link="/img/certificates/cert55.jpg" >}}
{{< /gallery >}}