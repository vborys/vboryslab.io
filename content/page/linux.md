---
title: GNU/Linux
subtitle: Операційні системи та їх адміністрування
bigimg: [{src: "/img/triangle_linux.jpg"}]
comments: false
---

### Linux Notes

Англомовний довідник спеціально розроблений та складений для початківців, які систематизують знання по GNU/Linux. Особливий пріоритет наданий командам налаштування Linux, сценаріям, сервісам та програмам.

[![alt](/img/projects/linux/pic01.png)](https://vborys.gitlab.io/linuxnotes//)

### Довідник по Void Linux

Ресурс, що описує встановлення та налаштування незалежної дистрибутивної версії операційної системи GNU/Linux, що базуєтся на бінарній пакетній системі xbps та системі ініціалізації runit.

[![alt](/img/projects/linux/pic02.png)](https://vbtutorials.gitlab.io/void-linux/)

### Open Solutions

Ця відео-збірка описує та демонструє процеси встановлення, налаштування та використання вільного програмного забезпечення в організації з невеликим парком ПК на основі операційних систем сімейства GNU/Linux.

[![alt](/img/projects/linux/pic03.png)](https://sites.google.com/view/opensolutions)

