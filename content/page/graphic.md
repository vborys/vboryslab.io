---
title: Графіка
subtitle: Комп'ютерні ілюстрації та макети
bigimg: [{src: "/img/triangle_graphic.jpg"}]
comments: false
---

### Графічні проєкти включають:
- дизайн логотипів
- візитки 
- банери
- постери
- інші друковані матеріали.

{{< gallery caption-effect="fade" >}}
  {{< figure thumb="" link="/img/projects/graphic/logo01.png" >}}
  {{< figure thumb="" link="/img/projects/graphic/logo02.png" >}}
  {{< figure thumb="" link="/img/projects/graphic/logo03.png" >}}
  {{< figure thumb="" link="/img/projects/graphic/logo04.png" >}}
  {{< figure thumb="" link="/img/projects/graphic/logo05.png" >}}
  {{< figure thumb="" link="/img/projects/graphic/logo06.png" >}}
  {{< figure thumb="" link="/img/projects/graphic/logo07.png" >}}
  {{< figure thumb="" link="/img/projects/graphic/logo08.png" >}}
  {{< figure thumb="" link="/img/projects/graphic/logo09.png" >}}
  {{< figure thumb="" link="/img/projects/graphic/logo10.jpg" >}}
{{< /gallery >}}

