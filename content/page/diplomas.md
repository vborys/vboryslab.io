---
title: Відзнаки
subtitle: Грамоти та подяки
comments: false
---

{{< gallery caption-effect="fade" >}}
  {{< figure thumb="" link="/img/diplomas/img1.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img2.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img3.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img4.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img5.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img6.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img7.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img8.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img9.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img10.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img11.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img12.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img13.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img14.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img15.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img16.jpg" >}}
  {{< figure thumb="" link="/img/diplomas/img17.jpg" >}}
{{< /gallery >}}