---
title: DevOps
subtitle: Практики та інструменти
bigimg: [{src: "/img/triangle_devops.jpg"}]
comments: false
---

### DevOps Tutorial

Англомовний довідник розроблений для систематизації знань з DevOps-практик та роботи основних інструментів.

[![alt](/img/projects/devops/pic01.png)](https://gitlab.com/vborys/devopscourse)


