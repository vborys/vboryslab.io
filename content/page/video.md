---
title: Відео-канали
subtitle: Створення навчального відео-контенту
bigimg: [{src: "/img/triangle_video.jpg"}]
comments: false
---

### Слідкуйте за мною на популярних відео-сервісах:


[![LBRY](/img/projects/video/lbry.png)](https://lbry.tv/@vborys:4)

[![YouTube](/img/projects/video/youtube.png)](https://www.youtube.com/channel/UCNThectI-RBKlKG6Mdq93Aw/videos)

