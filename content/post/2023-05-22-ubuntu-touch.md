---
title: Ubuntu Touch
subtitle: Встановлення та огляд на смартфоні Google Pixel 3a XL
date: 2023-05-22
tags: ["mobile", "ubuntu"]
---

![alt](/img/posts/ubuntu-touch.jpg)

## Прошивка телефону підтримуваною версією Android

Для доступу до можливості встановлення прошивки Ubuntu Touch смартфон повинен мати версію Android factory image PQ3B.190801.002:

Pixel 3a: https://developers.google.com/android/images#sargo

Pixel 3a XL: https://developers.google.com/android/images#bonito


Детальніше з описом і вимогами можна ознайомитись на [офіційному сайті](https://devices.ubuntu-touch.io/device/sargo)


### Підготовка 


* SDK Platform Tools [Скачати](https://developer.android.com/tools/releases/platform-tools)

* Factory Images for Pixel Devices [Скачати](https://developers.google.com/android/images)


### Порядок прошивки Android 9

* Увімкнути на телефоні Developer Mode

* Увімкнути в меню USB Debugging

* Увімкнути в меню OEM Unlocking

* Підключити смартфон до ПК (Allow USB Debuging)

* Перевантаження в завантажувач
```bash
adb reboot bootloader

```

* Розблокування завантажувача
```bash
fastboot flashing unlock
```


* Запуск скрипта прошивки
```bash
./flash-all.sh
```

Відеоінструкція - Pixel 3a Downgrade [Дивитись](https://youtu.be/ow5wCNp5itc)


## Прошивка телефону Ubuntu Touch 20.04

* Скачати [UBports Installer](https://devices.ubuntu-touch.io/installer/appimage)

![alt](/img/posts/ubports-installer1.png)

* Підключити смартфон до ПК (Allow USB Debuging)

* Вибрати встановлення Ubuntu Touch та версії 20.04


## Огляд системи

{{< gallery caption-effect="fade" >}}
  {{< figure thumb="" link="/img/posts/ubuntu_touch01.jpeg" >}}
  {{< figure thumb="" link="/img/posts/ubuntu_touch02.jpeg" >}}
  {{< figure thumb="" link="/img/posts/ubuntu_touch03.jpeg" >}}
  {{< figure thumb="" link="/img/posts/ubuntu_touch04.jpeg" >}}
  {{< figure thumb="" link="/img/posts/ubuntu_touch05.jpeg" >}}
  {{< figure thumb="" link="/img/posts/ubuntu_touch06.jpeg" >}}
  {{< figure thumb="" link="/img/posts/ubuntu_touch07.jpeg" >}}
{{< /gallery >}}



## Підключення до телевізора по WIFI в режимі DisplayMode

![alt](/img/posts/ubuntu_touch06.jpeg)
![alt](/img/posts/ubuntu_touch07.jpeg)



## Джерела

* Ubuntu Touch Overview - Google Pixel 3a [Дивитись ](https://www.youtube.com/watch?v=gpgUNv2XCEQ)

* Ubuntu Touch Installation - Google Pixel 3a (2022) [Дивитись ](https://www.youtube.com/watch?v=CdNGSdYCaDY)

* Ubuntu Touch 20.04 Overview - Google Pixel 3a (2023) [Дивитись ](https://www.youtube.com/watch?v=ltxj8YBoTMU)

* Used a Linux phone (2023) [Дивитись ](https://youtu.be/qmlabKbpTGM)

* Ubuntu Touch 20.04 - Wireless Display Cast (2023) [Дивитись ](https://youtu.be/ValkTii8KLg)

* Ubuntu Touch 20.04 - Installation [Дивитись ](https://www.youtube.com/watch?v=wUSsS5C3gUM)

## Розробка додатків для Ubuntu Touch

![alt](/img/posts/ubuntu_touch08.png)


* App Development (official) [Дивитись](https://docs.ubports.com/en/latest/appdev/index.html)

* Clickable [Дивитись](https://clickable-ut.dev/en/latest/)

* Suru icons [Дивитись](https://docs.ubports.com/projects/icons/en/latest/)

* An intro to mobile app development for Ubuntu Touch [Дивитись](https://ubports.gitlab.io/marketing/education/ub-clickable-1/index.html)

* How To Make An Ubuntu Touch Game[Дивитись](https://youtu.be/T7IHyDd4KKs)


* Developing by totodesbois100 [Дивитись](https://www.youtube.com/@totodesbois100/videos)