---
title: Робота з Network Manager
subtitle: Використання nmcli
date: 2020-12-15
tags: ["networks"]
---

### Загальні команди Network Manager

#### Перегляд ip
``` bash
ip addr show
```

#### Статистика
``` bash
ip -s link show
```

#### Таблиця маршрутизації
``` bash
ip route show match 0/0
```

### Загальні команди nmcli
``` bash
nmcli general status
nmcli device status
nmcli connection show
```

### UP & DOWN
``` bash
nmcli connection up id Connection1
nmcli connection down id Connection1

nmcli device connect eth0
nmcli device disconnect eth0
```

### З'єднання
``` bash
nmcli connection show Connection1
```

##### DHCP
``` bash
nmcli connection add type ethernet con-name "dhcp" ifname enp0s25
``` 

##### STATIC
``` bash
nmcli connection add type ethernet con-name Connection1 ifname enp0s25 ip4 10.0.0.100/24 gw4 10.0.0.1 ipv4.dns "8.8.8.8 8.8.4.4"
```

### Редагування
``` bash
nmlci connection modify Connection1 ipv4.addresses 192.168.2.20/24
nmcli con mod Connection1 ipv4.gateway 192.168.2.1
nmcli con mod Connection1 ipv4.dns “8.8.8.8”
```

##### Interactive mode
``` bash
nmcli connection edit Connection1
>save
```

### Видалення
``` bash
nmcli connection delete Connection1
```

### WIFI
``` bash
nmcli device

nmcli radio wifi on
nmcli radio wifi off

nmcli device wifi list

nmcli device wifi connect $WIFINETWORK password $PASS

nmcli connection delete $WIFINETWORK
```