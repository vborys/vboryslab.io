---
title: Налаштування Linux wifi-роутером
subtitle: Debian 8+dnsmasq+iptables+hostapd
date: 2020-12-19
tags: ["debian","networks"]
---

### Підготовка

В якості хоста виступає міні-ПК DELL Optiplex FX160 (Тонкий клієнт) 

Загальні характеристики:

* Intel® AtomTM 230 1,6 ГГц
* Dell Inc. 0TK7TF Motherboard
* Hyundai RAM 1Gb DDR2
* ATA Flash Di 512D PQ 2Gb
* SIS 771/671 PCIE VGA Display Adapter
* Broadcom Corporation NetXtreme BCM5764 Gigabit Ethernet PCIe
* Broadcom Corporation BCM4312 802.11b/g LP-PHY

[![alt](/img/posts/dell-optiplex-fx160.jpg)](https://www.dell.com/rs/business/p/optiplex-fx160/pd)

Встановлюваний дистрибутив [Debian GNU/Linux 8 Jessie](https://www.debian.org/releases/jessie/debian-installer/). 

Мережні інтерфейси 

* eth0 - вбудований Ethernet (WAN);

* wlan0 - BCM4312 802.11b/g (WLAN) ;



### 1. Настройка мережі

**/etc/network/interfaces**  виглядає так:

```bash
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp

auto wlan0
iface wlan0 inet static
  address 10.128.11.1
  network 10.128.11.0
  netmask 255.255.255.0
  broadcast 10.128.11.255 
```

Перезавантаження мережі:
```bash
sudo systemctl restart networking
```

### 2. Налаштування dnsmasq

Наступним кроком ставимо dnsmasq, який буде грати роль DHCP-сервера і кешуючого DNS-сервера:

```bash
sudo apt install dnsmasq dnsutils
```

**/etc/dnsmasq.conf**
```bash
server = 8.8.8.8
server = 8.8.4.4

domain-needed
bogus-priv

# Listen only given interfaces
interface = lo
interface = wlan0
bind-interfaces

# DNS cache size
cache-size = 10000

# IP ranges
dhcp-range = wlan0,10.128.11.50,10.128.11.250

# Default gateways
dhcp-option = wlan0,3,10.128.11.1

# DNS servers
dhcp-option = wlan0,6,10.128.11.1
```

Перезапуск процесів:

```bash
sudo systemctl restart dnsmasq
sudo systemctl status dnsmasq
```

### 3. Налаштування NAT за допомогою iptables

```bash
# Enable IP-forwarding
sudo sh -c 'echo 1> /proc/sys/net/ipv4/ip_forward'

# for WLAN
sudo iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
sudo iptables -A FORWARD -i eth0 -o wlan0 -m conntrack \
  --ctstate ESTABLISHED,RELATED -j ACCEPT

# allow traffic between LAN and WLAN
sudo iptables -A FORWARD -i eth1 -o wlan0 -j ACCEPT

sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
```

Якщо все працює, зберігаємо зроблені зміни, щоб вони не зникли після перезавантаження.

Для цього дописуємо в **/etc/sysctl.conf**:
```bash
net.ipv4.ip_forward = 1
```
#### Збереження правил iptables

```bash
sudo sh -c 'iptables-save> /etc/iptables.rules'
```

Створюємо скрипт **/etc/network/if-pre-up.d/iptables**:
```bash
#!/bin/sh
set -e
iptables-restore < /etc/iptables.rules
exit 0
```

Далі:
```bash
sudo chmod + x /etc/network/if-pre-up.d/iptables
```

Перезавантажуємо систему:
```bash
sudo reboot
```

### 4. Створення точки доступу з допомогою hostapd

```bash
sudo apt install hostapd
```

Правимо **/etc/default/hostapd**
```bash
DAEMON_CONF = "/etc/hostapd/hostapd.conf"
```

Створюємо файл **/etc/hostapd/hostapd.conf**:
```bash
interface=wlan0
ssid=MYWIFI
country_code=UA

hw_mode=g
channel=5

auth_algs=1

wpa=2
wpa_key_mgmt=WPA-PSK
rsn_pairwise=CCMP
wpa_passphrase=112233445566
```

SSID, пароль і номер каналу вказуємо власні)

Запускаємо hostapd:
```bash
sudo systemctl enable hostapd
sudo systemctl start hostapd
```

Повинна з'явитися точка доступу з SSID MYWIFI. Пробуємо до неї підключитися. Якщо раптом щось не працює, вивід можна отримати командою:
```bash
sudo hostapd -dd /etc/hostapd/hostapd.conf
```

### Кінець)
```bash
sudo reboot
```


### Джерела:

https://eax.me/raspberry-pi-wifi-router/

https://medium.com/@renaudcerrato/how-to-build-your-own-wireless-router-from-scratch-c06fa7199d95

https://gist.github.com/renaudcerrato/db053d96991aba152cc17d71e7e0f63c
