---
title: Debian 8 GNU/Linux
subtitle: Налаштування для старих ПК
date: 2019-09-01
tags: ["debian"]
---

![alt](/img/posts/banner_debian_8.jpg)

### Піcляінсталяційне налаштування

* Вибраний дистрибутив [Debian GNU/Linux 8 Jessie](https://www.debian.org/releases/jessie/debian-installer/).

* Графічне середовище XFCE4

#### Пошук найшвидшого дзеркала

```bash
su
sudo apt-get install netselect-apt
sudo netselect-apt
```

**/etc/apt/sources.list**
```bash
deb http://debian.org.ua/debian/ jessie main contrib non-free
deb-src http://debian.org.ua/debian/ jessie main contrib non-free

deb http://security.debian.org/ jessie/updates main 
deb-src http://security.debian.org/ jessie/updates main 

deb http://debian.org.ua/debian/ jessie-updates main contrib non-free
deb-src http://debian.org.ua/debian/ jessie-updates main contrib non-free

#deb http://httpredir.debian.org/debian jessie-backports main
```

#### Встановлення програмного забезпечення
```bash
sudo apt-get update
sudo apt-get upgrade
sudo reboot
sudo apt-get install build-essential debian-keyring mousepad network-manager \
network-manager-gnome p7zip unzip policykit-1-gnome p7zip-full aspell aspell-en \
hunspell hunspell-en-us mythes-en-us ristretto rsync thunar-archive-plugin \
fonts-dejavu fonts-dejavu-extra fonts-freefont-ttf fonts-liberation \
ttf-bitstream-vera ttf-dejavu ttf-dejavu-core ttf-dejavu-extra ttf-freefont \
ttf-liberation ttf-mscorefonts-installer ufw vlc xarchiver xfce4-clipman \
xfce4-panel-dev xfce4-power-manager xfce4-screenshooter xfce4-taskmanager \
xfce4-terminal xfce4-xkb-plugin xserver-xorg-input-synaptics 
```

#### Таймаут Grub2
```bash
sudo nano /etc/default/grub
TIMEOUT=0
```

#### Відображення користувача для lightdm
```bash
sudo nano /usr/share/lightdm/lightdm.conf.d/01_debian.conf
greeter-hide-users=false
```

#### Додаткові програми
```bash
sudo apt-get install synaptic gdebi htop
sudo apt-get install sane simple-scan
```

#### Тема Numix
```bash
sudo apt-get install numix-gtk-theme
sudo apt-get install http://ftp.us.debian.org/debian/pool/main/n/numix-icon-theme/numix-icon-theme_0~20180717-1_all.deb
```

#### Підключення пристроїв з Android
```bash
sudo apt-get install jmtpfs mtp-tools libudisks2-dev gvfs-backends && reboot
```