---
title: Debian (9,10,testing) after install
subtitle: З оболонками xfce4/i3-wm
date: 2020-10-01
tags: ["debian"]
---

### Редагування файлу з репозиторіями
```bash
nano /etc/apt/sources.list

     deb http://deb.debian.org/debian/ testing main contrib non-free
     deb-src http://deb.debian.org/debian/ testing main contrib non-free

     deb http://security.debian.org/debian-security/ testing-security main contrib non-free
     deb-src http://security.debian.org/debian-security/ testing-security main contrib non-free

     deb http://deb.debian.org/debian/ testing-updates main contrib non-free
     deb-src http://deb.debian.org/debian/ testing-updates main contrib non-free


apt update
apt upgrade
reboot
```

### Встановлення програм

>XFCE4 
```bash
apt-get install build-essential debian-keyring network-manager network-manager-gnome p7zip unzip policykit-1-gnome p7zip-full aspell aspell-en hunspell hunspell-en-us mythes-en-us ristretto rsync thunar-archive-plugin fonts-dejavu fonts-dejavu-extra fonts-freefont-ttf fonts-liberation ttf-bitstream-vera ttf-dejavu ttf-dejavu-core ttf-dejavu-extra ttf-freefont ttf-liberation ttf-mscorefonts-installer vlc xarchiver xfce4-clipman xfce4-panel-dev xfce4-power-manager xfce4-screenshooter xfce4-taskmanager xfce4-terminal xfce4-xkb-plugin xserver-xorg-input-synaptics intel-microcode bash-completion htop galculator kolourpaint4 git inxi exfat-utils 

sudo apt-get install numix-gtk-theme
sudo apt-get install http://ftp.debian.org/debian/pool/main/n/numix-icon-theme/numix-icon-theme_0~20190920-1_all.deb
```

>i3-wm 
```bash
sudo apt install xorg xserver-xorg xutils mesa-utils xinit firmware-linux firmware-linux-nonfree xserver-xorg-video-amdgpu mesa mesa-vulkan-drivers 

sudo apt install i3-wm i3lock i3status dmenu lxappearance lxrandr rxvt-unicode xterm nitrogen firefox-esr materia-gtk-theme pcmanfm deepin-icon-theme x11-xserver-utils lightdm lightdm-gtk-greeter ttf-ubuntu-font-family fonts-ubuntu-font-family-console fonts-ubuntu policykit-1-gnome galculator rofi scrot fonts-font-awesome compton xbacklight light p7zip unzip p7zip-full aspell aspell-en hunspell hunspell-en-us mythes-en-us xarchiver ristretto xserver-xorg-input-synaptics neofetch exfat-utils
```

### Налаштування мережі
```bash
sudo nano /etc/network/interfaces

        # This file describes the network interfaces available on your system
        # and how to activate them. For more information, see interfaces(5).

        source /etc/network/interfaces.d/*

        # The loopback network interface
        auto lo
        iface lo inet loopback

        # The primary network interface
        allow-hotplug enp1s0
        iface enp1s0 inet static
        address 192.168.1.110
        netmask 255.255.255.0
        network 192.168.1.1
        broadcast 192.168.255.255
        gateway 192.168.1.1

sudo systemctl restart networking
sudo reboot
```

### SUDO
```bash
apt install sudo
nano /etc/sudoers
```

### LIGHTDM
```bash
sudo nano /usr/share/lightdm/lightdm.conf.d/01_debian.conf
greeter-hide-users=false

sudo nano /etc/lightdm/lightdm-gtk-greeter.conf
[greeter]
background=/etc/lightdm/wall.jpg
theme-name=Numix
```

### GRUB
```bash
sudo nano /etc/default/grub
TIMEOUT=0
sudo update-grub
```

### LOW SWAPPINESS
```bash
sudo nano /etc/sysctl.conf
vm.swappiness = 10
```

### WPS Office
```bash
sudo cp -R uk_UA/ /opt/kingsoft/wps-office/office6/mui/
sudo cp -R dicts/uk_UA/ /opt/kingsoft/wps-office/office6/dicts/spellcheck/
sudo cp -R kstartpage/uk_UA/ /opt/kingsoft/wps-office/office6/addons/kstartpage/mui/
```

### ANDROID devices connect
```bash
sudo apt-get install jmtpfs mtp-tools libudisks2-dev gvfs-backends && reboot
```

### MULTIMEDIA
```bash
sudo apt install mpv
```

### PULSEAUDIO error FIX
```bash
sudo apt-get --purge --reinstall install pulseaudio
sudo ln -s /usr/bin/pavucontrol /usr/local/bin/xfce4-mixer
```

### CLEAN
```bash
sudo apt autoremove
```

### ZSH
```
sudo apt install zsh fonts-powerline
```
#### Oh my ZSH
>https://medium.com/tech-notes-and-geek-stuff/install-zsh-on-arch-linux-manjaro-and-make-it-your-default-shell-b0098b756a7a

>https://youtu.be/ZNHkS4EnXhQ



#### MPS-YOUTUBE terminal player
```bash
pip3 install --user mps-youtube
sudo apt install mplayer
/home/vborys/.local/bin/mpsyt
>set
---------------------------- 
  Key                 Value
  order             : relevance
  user_order        :
  max_res           : 2160p
  player            : mplayer
  playerargs        :
  encoder           : 0 [None]
  notifier          :
  checkupdate       : True
  show_mplayer_keys : True
  fullscreen        : False
  show_status       : True
  columns           :
  ddir              : /home/vborys/Downloads/mps
  overwrite         : True
*  show_video        : True
*  search_music      : False
  window_pos        :
  window_size       :
  download_command  :
  lastfm_username   :
  lastfm_password   :
  lastfm_api_key    :
  lastfm_api_secret :
  audio_format      : auto
  video_format      : auto
*  api_key           : AIzaSyB6WQWDNunguqB42z98njCASA2R2JMYkNM
  autoplay          : False
  set_title         : True
  mpris             : True
```

>https://github.com/mps-youtube/mps-youtube

>https://rtfm.co.ua/linux-mps-youtube-konsolnyj-pleer-youtube-fajlovplejlistov/

>https://zenway.ru/page/mpsyt



### QEMU/KVM
>https://www.youtube.com/watch?v=T8TzXwWMpyY

```bash
sudo apt install qemu-kvm libvirt-clients libvirt-daemon libvirt-daemon-system bridge-utils virtinst virt-manager -yy

sudo systemctl enable libvirtd
sudo systemctl start libvirtd
sudo adduser student libvirt
sudo adduser student libvirt-qemu
```

### SAMBA 
```bash
sudo apt-get install libcups2 samba samba-common cups
sudo mv /etc/samba/smb.conf /etc/samba/smb.conf.bak
sudo nano /etc/samba/smb.conf

        [global]
        workgroup=CLASS1
        server string=Samba Server %v
        netbios name=server
        security=user
        map to guest=bad user
        name resolve order=bcast host
        wins support=no
        dns proxy=no

        [pupils]
        path=/mnt/data/pupils
        browsable=yes
        writeble=yes
        guest ok=yes
        read only=no
        create mode=0777
        directory mode=0777

sudo systemctl restart smbd.service

#samba share in Thunar
sudo apt-get install gvfs-backends
```

### WIFI /rtl8723de/
```bash
sudo apt install dkms
git clone https://github.com/smlinux/rtl8723de.git -b current
dkms add ./rtl8723de
sudo dkms add ./rtl8723de
sudo dkms install rtl8723de/5.1.1.8_21285.20171026_COEX20170111-1414
sudo depmod -a
sudo reboot
```

### SCANER
```bash
sudo apt-get install sane simple-scan
```


