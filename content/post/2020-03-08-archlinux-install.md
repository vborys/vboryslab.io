---
title: Встановлення Arch Linux
subtitle: Чек-лист 2020
date: 2020-03-08
tags: ["archlinux"]
---

### Завантажитись з USB накопичувача

### Показ мережевих інтерфейсів

```bash
ip link show
systemctl start dhcpcd@enp0s16u3

ping -c 3 archlinux.org
```

### Робота з дисками

```bash
fdisk -l
cfdisk /dev/sda
/dev/sda1 # choose 512Mb of space (UEFI)
/dev/sda2 # choose at least 20 GB of space (root)
/dev/sda3 # choose all the left space (home)
lsblk

mkfs.fat -F32 /dev/sda1
mkfs.ext4 /dev/sda2
mkfs.ext4 /dev/sda3
mkswap

mount /dev/sda2 /mnt
mkdir /mnt/home
mount /dev/sda3 /mnt/home
swapon

lsblk
```
#### Встановлення системи
```bash
pacstrap -i /mnt base linux linux-firmware

genfstab -U  -p /mnt >> /mnt/etc/fstab

arch-chroot /mnt /bin/bash
```

#### Зміна локалізації та часового поясу
``` bash    
nano /etc/locale.gen
#Uncomment en_US.UTF-8 UTF-8 and other needed locales in /etc/locale.gen
locale-gen

nano /etc/locale.conf
        LANG=uk_UA.UTF-8
nano /etc/vconsole.conf
        KEYMAP=ua
        FONT=cyr-sun16


ln -sf /usr/share/zoneinfo/Europe/Kiev /etc/localtime

hwclock --systohc --utc

date 
```

#### Початкові налаштування системи

```bash
echo archbook > /etc/hostname

passwd

nano /etc/hosts
127.0.0.1    localhost
::1        localhost
127.0.1.1    archbook.localdomain    archbook

# NetworkManager
pacman -S networkmanager 
systemctl enable NetworkManager

```

#### Встановлення завантажувача
```bash
pacman -S grub efibootmgr amd-ucode
mkdir /boot/efi
mount /dev/sda1 /boot/efi
lsblk         # to check if everything is mounted correctly
grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi --removable
grub-mkconfig -o /boot/grub/grub.cfg
```

#### Timeout
```bash
nano /etc/default/grub
        timeout=0
```

#### Перезавантаження
```bash
exit

umount -R /mnt
reboot
```

#### Swap file
```bash
fallocate -l 3G /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo '/swapfile none swap sw 0 0' >> /etc/fstab
free -m
```

#### Створення облікових записів користувачів
```bash
useradd -m -g users -G wheel -s /bin/bash <user>
passwd <user>
pacman -S sudo
nano /etc/sudoers
%wheel...
```

#### Встановлення системних програм
```bash
pacman -S pulseaudio pulseaudio-alsa xorg xorg-xinit xorg-server

??linux-firmware linux-firmware-amd
>>OLD
sudo pacman -S pulseaudio pulseaudio-alsa alsa-lib alsa-utils

sudo pacman -S xf86-video-amdgpu

sudo nano /etc/X11/xorg.conf.d/20-amdgpu.conf
Section "Device"
    Identifier "AMD"
    Driver "amdgpu"
    Option "TearFree" "true"
EndSection

```

#### Встановлення DE
>i3-wm
```bash
sudo pacman -S i3-gaps i3status dmenu setxkbmap st 
```
>xfce4
```bash
sudo pacman -S xfce4 xfce4-goodies xfce4-whiskermenu-plugin
echo "exec startxfce4" > ~/.xinitrc
yay -S xfce4-places-plugin numix-themes-archblue numix-square-icon-theme-git plank plank-theme-numix numix-circle-arc-icons-git
```

#### Lightdm
```bash
sudo pacman -S lightdm lightdm-gtk-greeter
systemctl enable lightdm
```

### Встановлення додаткових драйверів
```bash
#************ rtl8723de wifi *******************
sudo pacman -S git dkms linux-headers
git clone -b extended --single-branch https://github.com/lwfinger/rtlwifi_new.git
cd rtlwifi_new/
make
sudo make install
sudo /bin/sh -c 'echo "options rtl8723de ant_sel=2" >> /etc/modprobe.d/rtl8723de.conf'
```

#### Створення підключення в nmcli
```bash
nmcli d
nmcli r wifi on
nmcli d wifi list
nmcli d wifi connect antena password *****

nmcli connection delete antena
```

### Встановлення програм
#### Вручну
```bash
sudo pacman -S --needed base-devel
sudo nano /etc/makepkg.conf
    MAKEFLAGS=”-j $(nproc)”
nproc
mkdir Programs
cd Programs/

#yay AUR helper
git clone https://aur.archlinux.org/yay.git
cd yay/
ls
cat PKGBUILD
makepkg -si      //або   makepkg -s && sudo pacman -U yay-***
```

#### З допомогою pacman
```bash
sudo pacman -S gvfs gvfs-mtp file-roller p7zip galculator
sudo pacman -S ttf-ubuntu-font-family ttf-dejavu ttf-liberation 
sudo pacman -S screenfetch inxi net-tools mpv
sudo pacman -S firefox firefox-i18n-uk
```

#### З допомогою yay
```bash
yay -S ttf-ms-fonts
yay -S wps-office
```

#### TLP battery service
```bash
sudo pacman -S tlp
sudo systemctl enable tlp.service
sudo systemctl start tlp.service
```

#### Less logs
```bash
sudo nano /etc/systemd/journald.conf
SystemMaxUse=5M

sudo systemctl disable man-db.service
sudo systemctl disable man-db.timer
```

#### WPS Office locale in archlinux
/home/vborys/.kingsoft/office6/mui
