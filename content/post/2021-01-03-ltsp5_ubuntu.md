---
title: LTSP5 на Ubuntu 14.04
subtitle: Встановлення та налаштування термінального серверу
date: 2021-01-03
tags: ["ltsp","ubuntu"]
---

![alt](/img/posts/ltsp.jpg)



* Офіційний сайт [Linux Terminal Server Project](https://ltsp.org/).

### Встановлення LTSP5
```bash
sudo apt install ltsp-server-standalone openssh-server
```
### Налаштування мережі
```bash
sudo nano /etc/network/interfaces

    auto eth0
    iface eth0 inet static
    address 192.168.0.107
    netmask 255.255.255.0
    broadcast 192.168.0.255
    gateway 192.168.0.1
    dns-nameservers 8.8.8.8
```
### Збірка клієнтського образу
```bash
sudo ltsp-build-client --arch amd64
```

### Налаштування tftp-серверу
```bash
sudo nano /etc/default/tftpd-hpa

    TFTP_USERNAME="tftp"
    TFTP_DIRECTORY="/var/lib/tftpboot"
    TFTP_ADDRESS="192.168.0.107:69"
    TFTP_OPTIONS="--secure"
```

### Конфігурація NetworkManager
```bash
sudo nano /etc/NetworkManager/NetworkManager.conf

[ifupdown]
managed=true
```

### Настройка DHCP-серверу
```bash
sudo nano /etc/dhcp/dhcpd.conf


    option domain-name-servers 8.8.8.8;

    sudnet 192.168.0.0 netmask 255.255.255.0{
            range 192.168.0.110 192.168.0.250;
            option routers 192.168.0.1;
    }

    include "/etc/ltsp/dhcpd.conf";
```

### DHCP-сервер LTSP
```bash
sudo nano /etc/ltsp/dhcpd.conf

    #
    # Default LTSP dhcpd.conf config file.
    #

    authoritative;

    subnet 192.168.0.0 netmask 255.255.255.0 {
        range 192.168.0.110 192.168.0.250;
        option domain-name "example.com";
        option domain-name-servers 8.8.8.8;
        option broadcast-address 192.168.0.255;
        option routers 192.168.0.1;
    #    next-server 192.168.0.1;
    #    get-lease-hostnames true;
        option subnet-mask 255.255.255.0;
        option root-path "/opt/ltsp/amd64";
        if substring( option vendor-class-identifier, 0, 9 ) = "PXEClient" {
            filename "/ltsp/amd64/pxelinux.0";
        } else {
            filename "/ltsp/amd64/nbi.img";
        }
    }

```

### Додаткові налаштування
```bash
sudo nano /etc/default/isc-dhcp-server

    INTERFACES="eth0"
```

```bash
sudo reboot
```

### Додавання користувачів LTSP
```bash
useradd test
passwd test
```


### Команди керування
```bash
sudo service isc-dhcp-server restart
sudo ltsp-update-image
```

#### for Debian
```bash
sudo nfs-kernel-server restart
```

#### for Ubuntu 
```bash
sudo nbd-server restart
```

### Джерела

* Встановлення [LTSP5 на Ubuntu 14.04](https://www.youtube.com/watch?v=cwodv7y55hg)

* Встановлення [LTSP5 на Debian](https://youtu.be/lv6C3DUGZi4)
