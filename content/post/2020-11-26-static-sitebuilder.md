---
title: Генератори сайтів
subtitle: Огляд MkDocs i Hugo
date: 2020-11-25
tags: ["site-generator"]
---

### MkDocs
Швидкий і простий статичний генератор сайтів написаний на мові Python, який спрямований на створення проектної документації. Вихідні файли документації записуються в Markdown і конфігуруються з одним файлом конфігурації YAML. 

#### Ресурси в мережі Інтернет

[Головний сайт проекту MKDocs](https://www.mkdocs.org/)

[Опис плагіну mkdocs-material](https://squidfunk.github.io/mkdocs-material/)

[Створення сайту докуметації з MkDocs+GitLabPages част.1 (відео від Rafael Galleani)](https://youtu.be/k7rkjVfuB2M)

[Створення сайту докуметації з MkDocs+GitLabPages част.2 (відео від Rafael Galleani)](https://youtu.be/oDAHnwmPfjA)


### Hugo
Один з найпопулярніших генераторів статичних сайтів з відкритим кодом (написаний на мові Go).

[Головний сайт проекту Hugo](https://gohugo.io/)

[Сторінка тем проєкту Hugo](https://themes.gohugo.io/)



#### Публікація сайту на GitLab

[Створення сайту за допомогою Hugo і Gitlab (відео від Rwaltr)](https://www.youtube.com/watch?v=-q6ZiCroiGM)

[Стаття: Сайт на Hugo](https://maxpowerwastaken.gitlab.io/model-idiot/posts/launching-a-hugo-site-for-free-on-gitlab-pages/)
